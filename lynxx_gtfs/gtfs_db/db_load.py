import zipfile
import tempfile
import logging
log = logging.getLogger(__name__)
from contextlib import closing
import time
import shutil
from gtfs_app.models import Stop, StopTimes
import csv

class GTFS (object):
    def __init__(self) -> None:
        super().__init__()

    def unzip (self, filename, path=None):
        self.local_file = filename
        path = path if path else tempfile.mkdtemp()
        try:
            with closing(zipfile.ZipFile(self.local_file)) as z:
                z.extractall(path)
        except Exception as e:
            log.warning(e)
        return path

    def mapper_header_to_dict (self, csv_headings):
        self.mapped_dict={v:k for (k,v) in enumerate(csv_headings)}

    def load_to_db (self,zip):
        with tempfile.TemporaryDirectory() as path:
            unzipped_folder = self.unzip(zip, path)
            with open (unzipped_folder+'/test/data/stops.txt') as csvfile:
                gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
                csv_headings = next(gtfs_data)
                self.mapper_header_to_dict(csv_headings)
                for single_row in gtfs_data:
                    stop : Stop = Stop.objects.create_from_csv (single_row, self.mapped_dict)
            with open (unzipped_folder+'/test/data/stop_times.txt') as csvfile:
                gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
                csv_headings = next(gtfs_data)
                self.mapper_header_to_dict(csv_headings)
                for single_row in gtfs_data:
                    stopTimes : StopTimes = StopTimes.objects.create_from_csv_StopTimes (single_row, self.mapped_dict)
            

