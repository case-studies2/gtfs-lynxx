from django.shortcuts import render
from django.template import loader
from django.http import HttpResponseRedirect, HttpResponse
from .forms import UploadFileForm
from gtfs_db import db_load
import os
from django.views.generic import ListView

# Create your views here.
def index(request):
    template = loader.get_template('gtfs_app/index.html')
    context = {
        'latest_question_list': [],
    }
    return HttpResponse(template.render(context, request))

from django.core.files.storage import FileSystemStorage
def upload_file(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        Stop.objects.all().delete()
        StopTimes.objects.all().delete()
        gtfs_database = db_load.GTFS ()
        gtfs_database.load_to_db (filename)
        os.remove(filename)
        return render(request, 'gtfs_app/upload_file.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'gtfs_app/upload_file.html')

from gtfs_app.models import Stop, StopTimes
from django.core.paginator import Paginator

def display_gtfs_content (request):
    stops = Stop.objects.all ()
    paginator = Paginator(stops, 25) # Show 25 per page.

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, "gtfs_app/display_gtfs_content.html",{'page_obj': page_obj})

def display_gtfs_stopTimes (request, id):
    stopTimes = StopTimes.objects.filter (stop_id=id)

    day_filter = request.GET.get('day')
    if day_filter:
        stopTimes = stopTimes.filter(trip_id__contains=f"{day_filter}")

    paginator = Paginator(stopTimes, 25) # Show 25 per page.
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, "gtfs_app/display_gtfs_stopTimes.html",{'page_obj': page_obj})
