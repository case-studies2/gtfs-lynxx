from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name='index'),
    path('upload_file', views.upload_file, name='upload_file'),
    path('display_gtfs_content', views.display_gtfs_content, name='display_gtfs_content'),
    path('display_gtfs_stopTimes', views.display_gtfs_stopTimes, name='display_gtfs_stopTimes'),
    path('display_gtfs_stopTimes/<int:id>/', views.display_gtfs_stopTimes, name='display_gtfs_stopTimes'),
]