from django.test import TestCase
from gtfs_app.models import Stop, StopTimes
from gtfs_db import db_load
from pathlib import Path
import shutil
import csv

import gtfs_db

# Create your tests here.

# # Test 1.1: Can create an instance of the Stop object, and insert into
# # the database, and retrieve it
# class TestStopModel(TestCase):
#     def setUp(self) -> None:
#         stop = Stop.objects.create(stop_id='2', stop_code='asdf', stop_name='asdf', stop_desc='asdf')
        
#     def test_can_create_a_stop_object(self):
#         l : Stop = Stop.objects.get(stop_id='2')
#         self.assertEqual(l.stop_id, '2')


# # Test 1.2: Unzip the uploaded file into a temporary directory
# class TestGTFSProcessing(TestCase):
#     def setUp(self) -> None:
#         self.zip = "/Users/birdy/workspace/lynxx_django_gtfs/lynxx_gtfs/gtfs_db/test.zip"
#         gtfs_inst = db_load.GTFS ()
#         self.unzipped_folder = gtfs_inst.unzip(self.zip)

#     def tearDown(self) -> None:
#         shutil.rmtree(self.unzipped_folder)

#     def test_unzip_gtfs (self):
#         self.assertTrue(Path.exists(Path(self.unzipped_folder)))

#     # Test 1.3: Read the first stop from the CSV
#     def test_loading_line_csv (self):
#         # import os
#         # path = unzipped_folder
#         # for root, directories, files in os.walk(path, topdown=False):
# 	    #     for name in files:
# 		#         print(os.path.join(root, name))
# 	    #     for name in directories:
# 		#         print(os.path.join(root, name))
#         with open (self.unzipped_folder+'/test/data/stops.txt') as csvfile:
#             gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
#             csv_headings = next(gtfs_data)
#             first_line = next(gtfs_data)
#             print (first_line)
#             # for row in gtfs_data:
#             #     print(', '.join(row))

        

#     # Test 1.4: load the first line of csv into the Test DB
#     def test_loading_line_csv (self):

#         # import os
#         # path = unzipped_folder
#         # for root, directories, files in os.walk(path, topdown=False):
# 	    #     for name in files:
# 		#         print(os.path.join(root, name))
# 	    #     for name in directories:
# 		#         print(os.path.join(root, name))
#         with open (self.unzipped_folder+'/test/data/stops.txt') as csvfile:
#             gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
#             csv_headings = next(gtfs_data)
#             first_line = next(gtfs_data)
#             # for row in gtfs_data:
#             #     print(', '.join(row))
#             stop = Stop.objects.create(stop_id=first_line[0])
#             print(Stop.objects.values_list('stop_id',flat=True))


#     # Test 1.5: Determine the map of name of to column of the CSV
#     #         i.e col 0 is stop_id -> col['stop_id'] = 0
#     #         note, you would need to assume the column names
#     #         are probably correct
#     def test_map_name (self):
#         with open (self.unzipped_folder+'/test/data/stops.txt') as csvfile:
#             gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
#             csv_headings = next(gtfs_data)
#             gtfs_inst = db_load.GTFS ()
#             gtfs_inst.mapper_header_to_dict(csv_headings)
#             self.assertEqual (gtfs_inst.mapped_dict["stop_id"],0)

#     # Test 1.6: Load a single csv row into the Stop object
#     def test_load_single_csv_stop (self):
#         with open (self.unzipped_folder+'/test/data/stops.txt') as csvfile:
#             gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
#             csv_headings = next(gtfs_data)
#             single_csv = next (gtfs_data)
#             gtfs_inst = db_load.GTFS ()
#             gtfs_inst.mapper_header_to_dict(csv_headings)
#             stop : Stop = Stop.objects.create_from_csv (single_csv, gtfs_inst.mapped_dict)
        
#             self.assertEqual(stop.stop_id, '1')
#             self.assertEqual(stop.stop_code, '000001')
#             self.assertEqual(stop.stop_name, 'Herschel Street Stop 1 near North Quay')
#             self.assertEqual(stop.stop_desc, '')

#     # Test 1.7: load the all stop_id values into the Test DB
#     def test_loading_full_csv (self):
#         with open (self.unzipped_folder+'/test/data/stops.txt') as csvfile:
#             gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
#             csv_headings = next(gtfs_data)
#             gtfs_inst = db_load.GTFS ()
#             gtfs_inst.mapper_header_to_dict(csv_headings)
#             for single_row in gtfs_data:
#                 stop : Stop = Stop.objects.create_from_csv (single_row, gtfs_inst.mapped_dict)
#             print ('loaded full csv')
#             print(Stop.objects.values_list('stop_name',flat=True))

#     def test_db_load_csv_to_db (self):
#         gtfs_inst = db_load.GTFS ()
#         gtfs_inst.load_to_db (self.zip)
#         print ("db_load test passed")
        
# Test 2.1: Can create an instance of the Stop Times object, and insert into
# the database, and retrieve it
class TestStopTimesModel(TestCase):
    def setUp(self) -> None:
        stop = StopTimes.objects.create(trip_id='asdf', arrival_time='09:26:03.478039', departure_time='21:45:04.778739', stop_id='somenum', stop_sequence ='456')
        
    def test_can_create_a_stop_object(self):
        s : StopTimes = StopTimes.objects.get(trip_id='asdf')
        self.assertEqual(s.trip_id, 'asdf')

# Test 2.2: Unzip the uploaded file into a temporary directory
class TestGTFSProcessingStopTimes(TestCase):
    def setUp(self) -> None:
        self.zip = "/Users/birdy/workspace/lynxx_django_gtfs/lynxx_gtfs/gtfs_db/test.zip"
        gtfs_inst = db_load.GTFS ()
        self.unzipped_folder = gtfs_inst.unzip(self.zip)

    def tearDown(self) -> None:
        shutil.rmtree(self.unzipped_folder)

    def test_unzip_gtfs (self):
        self.assertTrue(Path.exists(Path(self.unzipped_folder)))

    # Test 2.3: Read the first stop from the CSV
    def test_loading_line_csv_StopTimes (self):
        # import os
        # path = unzipped_folder
        # for root, directories, files in os.walk(path, topdown=False):
	    #     for name in files:
		#         print(os.path.join(root, name))
	    #     for name in directories:
		#         print(os.path.join(root, name))
        with open (self.unzipped_folder+'/test/data/stop_times.txt') as csvfile:
            gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
            csv_headings = next(gtfs_data)
            first_line = next(gtfs_data)
            print (first_line)
            # for row in gtfs_data:
            #     print(', '.join(row))
    # Test 1.4: load the first line of csv into the Test DB
    def test_loading_line_csv_StopTimes (self):

        # import os
        # path = unzipped_folder
        # for root, directories, files in os.walk(path, topdown=False):
	    #     for name in files:
		#         print(os.path.join(root, name))
	    #     for name in directories:
		#         print(os.path.join(root, name))
        with open (self.unzipped_folder+'/test/data/stop_times.txt') as csvfile:
            gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
            csv_headings = next(gtfs_data)
            first_line = next(gtfs_data)
            # for row in gtfs_data:
            #     print(', '.join(row))
            stop_times = StopTimes.objects.create(trip_id=first_line[0], arrival_time=first_line[1], departure_time=first_line[2], stop_id=first_line[3], stop_sequence =first_line[4])
            print(StopTimes.objects.values_list('trip_id',flat=True))
    # Test 2.5: Determine the map of name of to column of the CSV
    def test_map_nameStopTimes (self):
        with open (self.unzipped_folder+'/test/data/stop_times.txt') as csvfile:
            gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
            csv_headings = next(gtfs_data)
            gtfs_inst = db_load.GTFS ()
            gtfs_inst.mapper_header_to_dict(csv_headings)
            self.assertEqual (gtfs_inst.mapped_dict["trip_id"],0)
    # Test 2.6: Load a single csv row into the StopTimes object
    def test_load_single_csv_StopTimes (self):
        with open (self.unzipped_folder+'/test/data/stop_times.txt') as csvfile:
            gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
            csv_headings = next(gtfs_data)
            single_csv = next (gtfs_data)
            gtfs_inst = db_load.GTFS ()
            gtfs_inst.mapper_header_to_dict(csv_headings)
            stopTimes : StopTimes = StopTimes.objects.create_from_csv_StopTimes(single_csv, gtfs_inst.mapped_dict)
        
            self.assertEqual(stopTimes.trip_id, '17323011-BBL 21_22-BBL_WD-Weekday-01')
            self.assertEqual(stopTimes.arrival_time, '06:30:00')
            self.assertEqual(stopTimes.departure_time, '06:30:00')
            self.assertEqual(stopTimes.stop_id, '12096')
            self.assertEqual(stopTimes.stop_sequence, '1')

    # Test 1.7: load the all stop_id values into the Test DB
    def test_loading_full_csvStopTimes (self):
        with open (self.unzipped_folder+'/test/data/stop_times.txt') as csvfile:
            gtfs_data = csv.reader(csvfile, delimiter=',', quotechar='"')
            csv_headings = next(gtfs_data)
            gtfs_inst = db_load.GTFS ()
            gtfs_inst.mapper_header_to_dict(csv_headings)
            # for single_row in gtfs_data:
            #     stopTimes : StopTimes = StopTimes.objects.create_from_csv_StopTimes (single_row, gtfs_inst.mapped_dict)
            print ('loaded full csv')
            print(StopTimes.objects.values_list('trip_id',flat=True))

    def test_db_load_csv_to_db (self):
        gtfs_inst = db_load.GTFS ()
        gtfs_inst.load_to_db (self.zip)
        print ("db_load test passed")

# need to put back TimeFormat - removed because date could not be resolved 