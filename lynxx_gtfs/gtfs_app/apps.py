from django.apps import AppConfig


class GtfsAPPConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gtfs_app'
