from django.db import models

# for Stop model
class StopManager(models.Manager):
    def createStop(self, **kwargs):
        stop = self.create(**kwargs)
        return stop
        
    def create_from_csv (self, csv_row, csv_name_to_index):
        stop_id = csv_name_to_index['stop_id']
        stop_code = csv_name_to_index['stop_code']
        stop_name = csv_name_to_index['stop_name']
        stop_desc = csv_name_to_index['stop_desc']
        stop = Stop.objects.create(stop_id = csv_row[stop_id], stop_code = csv_row[stop_code], stop_name = csv_row[stop_name], stop_desc = csv_row[stop_desc])
        return stop

# for Stop Times model
class StopTimesManager(models.Manager):
    def createStopTimes(self, **kwargs):
        stop_times = self.create(**kwargs)
        return stop_times

    def create_from_csv_StopTimes (self, csv_row, csv_name_to_index):
        trip_id = csv_name_to_index['trip_id']
        arrival_time = csv_name_to_index['arrival_time']
        departure_time = csv_name_to_index['departure_time']
        stop_id = csv_name_to_index['stop_id']
        stop_sequence = csv_name_to_index['stop_sequence']
        stopTimes = StopTimes.objects.create(trip_id = csv_row[trip_id], arrival_time = csv_row[arrival_time], departure_time = csv_row[departure_time], stop_id = csv_row[stop_id],stop_sequence = csv_row[stop_sequence])
        return stopTimes

# Create your models here.
# model to create Stops from stops.txt
class Stop(models.Model):
    stop_id = models.CharField(max_length = 30)
    stop_code = models.CharField(max_length = 30, null=True)
    stop_name = models.TextField(null=True)
    stop_desc = models.TextField(null=True)

    objects = StopManager()

# model to create Stop Times from stop_times.txt
class StopTimes (models.Model):
    trip_id = models.CharField(max_length = 30)
    arrival_time = models.CharField(max_length = 30)
    departure_time = models.CharField(max_length = 30)
    stop_id = models.CharField(max_length = 30)
    stop_sequence = models.IntegerField(max_length = 30)

    objects = StopTimesManager()
