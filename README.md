GTFS uploader to access stops and filtered timetable

You can find an example GTFS test.zip file in lynxx_gtfs which you can use to test the system.

You need to go to /gtfs_app/upload_files to see the functionality.

It will take a while to upload the files. Once the files are uploaded into the database the link to display stops will be visible. 
